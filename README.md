# Pattern Builder Previewer

Provides a rendered preview of a patterns while on an entity form.

# Installation

* Install as usual, see https://drupal.org/node/895232 for further information.
* Navigate to Administer >> Module: Enable "Pattern Builder Previewer".
* Create / Edit a paragraphs field that is used for Pattern Builder:
  * Set widget to "Embedded Patterns with Previewer"
  * Set "Default edit mode" to "Closed" or "Preview".

# Requirements

* Pattern Builder module (https://www.drupal.org/project/patternbuilder)
* Paragraphs Previewer module (https://www.drupal.org/project/paragraphs_previewer)
